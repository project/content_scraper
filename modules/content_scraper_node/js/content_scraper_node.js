(function ($) {

    Drupal.behaviors.content_scraper_node = {
        attach: function (context) {
            $('fieldset.node-form-scraper-options', context).drupalSetSummary(function (context) {
                var vals = [];

                $('input:checked', context).parent().each(function () {
                    vals.push(Drupal.checkPlain($.trim($(this).text())));
                });

                if (!$('.form-item-scraper-node-active input', context).is(':checked')) {
                    vals.unshift(Drupal.t('Disabled'));
                }
                return vals.join(', ');
            });
        }
    };

})(jQuery);
