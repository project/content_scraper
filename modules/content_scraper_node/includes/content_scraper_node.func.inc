<?php

/**
 * @file
 * Provides helper function.
 */

/**
 * @param $node
 * @return bool
 */
function _content_scraper_node_check($node) {
  if (!_content_scraper_node_bundle_check($node->type)) {
    return FALSE;
  }

  // If this node is marked as not active for content scraper.
  if ($node->scraper_node_active !== CONTENT_SCRAPER_NODE_ACTIVE) {
    // Save into mapping table.
    _content_scraper_node_save_mapping($node);
    return FALSE;
  }

  return _content_scraper_node_revision_check($node);
}

/**
 * @param $entity
 * @return bool
 */
function _content_scraper_node_revision_check($entity) {
  // Workbench Moderation module Integration.
  if (!module_exists('workbench_moderation')) {
    return TRUE;
  }
  elseif (workbench_moderation_node_type_moderated($entity->type) === TRUE && empty($entity->workbench_moderation['updating_live_revision'])) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}

/**
 * Check current bundle.
 * @param $bundle
 * @return bool
 */
function _content_scraper_node_bundle_check($bundle) {
  $node_types = content_scraper_node_bundles();
  $bundles = array_keys($node_types);
  if (in_array($bundle, $bundles)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Return fields under current node type.
 * @param $type
 * @return array
 */
function _content_scraper_node_get_fields_by_type($type) {
  $fields =  &drupal_static(__FUNCTION__);

  if (!isset($fields)) {
    $fields = array();
    $node_types = content_scraper_node_bundles();
    if (array_key_exists($type, $node_types)) {
      $fields = $node_types[$type];
    }
  }

  return $fields;
}

/**
 * Return nids with content scraper link integration enabled.
 * @return mixed
 */
function _content_scraper_node_get_nids() {
  $node_types = array_keys(content_scraper_node_bundles());
  if (!empty($node_types)) {
    $result = db_select('node', 'n')
      ->fields('n', array('nid', 'type', 'status'))
      ->condition('type', $node_types, 'IN')
      ->condition('status', '1')
      ->execute();
    $record = $result->fetchCol();
    $total = $result->rowCount();
    if ($total > 0) {
      return $record;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * @param $entity
 * @param $field_name
 * @return string|void
 */
function _content_scraper_node_fetch_content_by_nid($nid, $field_name = 'content_scraper_body') {
  if (empty($nid)) {
    return FALSE;
  }

  // Fetch content scraper content entity.
  // Load module inc file.
  module_load_include('inc', 'content_scraper', 'includes/content_scraper.handler');
  $sids = content_scraper_link_fetch_sids_by_entity_id($nid, 'node');
  if (empty($sids)) {
    return FALSE;
  }

  // @todo: allow how many content scraper reference per node.
  reset($sids);
  $sid = end($sids);
  $cs_entity = content_scraper_content_load($sid);
  // Return if no content.
  if (empty($cs_entity) || empty($cs_entity->content_scraper_body)) {
    return FALSE;
  }

  $cs_entity_wrapper = entity_metadata_wrapper('content_scraper_content', $cs_entity);
  $cs_field_value = $cs_entity_wrapper->$field_name->value();

  return !empty($cs_field_value) ? $cs_field_value : FALSE;
}

/**
 * Extract links from node.
 * @param $node
 * @return bool
 */
function _content_scraper_node_extract_links($node) {
  $fields = _content_scraper_node_get_fields_by_type($node->type);
  $link_fields = array_keys($fields);
  if (!empty($link_fields)) {
    $links = array();
    $node_wrapper = entity_metadata_wrapper('node', $node);
    foreach ($link_fields as $link_field) {
      if (isset($node_wrapper->$link_field)) {
        $links_array = $node_wrapper->$link_field->value();
        if (empty($links_array)) {
          continue;
        }
        if (isset($links_array[0])) {
          foreach ($links_array as $link_array) {
            $link_array['type'] = $fields[$link_field]['type'];
            $link_array['status'] = isset($node->scraper_node_active) ? $node->scraper_node_active : CONTENT_SCRAPER_NODE_ACTIVE;
            $links[] = $link_array;
          }
        }
        else {
          $links_array['type'] = $fields[$link_field]['type'];
          $links_array['status'] = isset($node->scraper_node_active) ? $node->scraper_node_active : CONTENT_SCRAPER_NODE_ACTIVE;
          $links[] = $links_array;
        }
      }
    }
    return $links;
  }
  else {
    return FALSE;
  }
}

/**
 * Save link within node.
 * @param $link
 * @param $node
 * @return bool
 */
function _content_scraper_node_save_link($link, $node) {
  if (empty($link['url'])) {
    return FALSE;
  }

  // Load module inc file.
  module_load_include('inc', 'content_scraper', 'includes/content_scraper.handler');

  $extras = array(
    'entity_id' => empty($node->nid) ? NULL : $node->nid,
    'entity_type' => 'node',
    'entity_bundle' => empty($node->type) ? NULL : $node->type,
    'status' => isset($node->scraper_node_active) ? $node->scraper_node_active : CONTENT_SCRAPER_NODE_ACTIVE,
  );
  return content_scraper_link_save_link($link, $extras);
}

/**
 * Save into content scraper mapping table.
 * @param $node
 */
function _content_scraper_node_save_mapping($node) {
  if (!_content_scraper_node_bundle_check($node->type)) {
    return FALSE;
  }
  // Save into links at first.
  $links = _content_scraper_node_extract_links($node);
  if (is_array($links) && !empty($links)) {
    // Filter array without url.
    foreach ($links as $k => $link) {
      if (empty($link['url']) || !valid_url($link['url'], TRUE)) {
        continue;
      }
      $link['lid'] = _content_scraper_node_save_link($link, $node);
      $query_sid = db_query('SELECT entity_id FROM {content_scraper_link_mapping} WHERE link_id = :link_id AND entity_type = :entity_type', array(
        ':link_id' => $link['lid'],
        ':entity_type' => 'content_scraper_content'
      ));
      // Only create content entity if not exists and type is valid.
      if ($query_sid->rowCount() == 0 && content_scraper_content_type_load($link['type'])) {
        // Create a new content scraper content.
        $cs_entity_content = content_scraper_content_create($link['type']);
        $cs_entity_content->label = !empty($node->title) ? $node->title : 'Link from node';
        $cs_entity_content->language = !empty($node->language) ? $node->language : LANGUAGE_NONE;
        $cs_entity_content->uid = !empty($node->uid) ? $node->uid : '0';
        $cs_entity_content->status = !empty($node->status) ? $node->status : 1;
        $cs_entity_wrapper = entity_metadata_wrapper('content_scraper_content', $cs_entity_content);

        $cs_entity_wrapper->content_scraper_url->set(array(
          'url' => $link['url'],
          'title' => !empty($link['title']) ? $link['title'] : '',
        ));
        $cs_entity_wrapper->save();
        $sid = $cs_entity_wrapper->sid->value();

        if ($sid) {
          db_merge('content_scraper_link_mapping')
            ->key(array(
              'link_id' => $link['lid'],
              'entity_id' => $sid,
              'entity_type' => 'content_scraper_content',
            ))
            ->fields(array(
              'link_id' => $link['lid'],
              'entity_id' => $sid,
              'entity_type' => 'content_scraper_content',
              'entity_bundle' => $link['type'],
              'status' => $link['status'],
            ))
            ->execute();
        }
      }
    }
  }
}

/**
 * Update content scraper entity.
 * @param $node
 */
function content_scraper_node_update_entity($node) {
  // Save into mapping table.
  _content_scraper_node_save_mapping($node);
}

/**
 * Delete content scraper entity.
 * @param $node
 */
function content_scraper_node_delete_entity($nid) {
  if (empty($nid)) {
    return;
  }
  // Delete content scraper entity.
  $entity_query = db_query('SELECT `cslm`.`link_id` FROM `content_scraper_link_mapping` cslm
                            WHERE `cslm`.`link_id` IN (SELECT `link_id` FROM `content_scraper_link_mapping` WHERE `entity_id` = :entity_id AND `entity_type` = :entity_type)
                            AND `cslm`.`entity_type` <> :scraper_entity_type
                            GROUP BY `cslm`.`link_id` HAVING (count(`cslm`.`entity_id`) = 1)', array(
    ':entity_id' => $nid,
    ':entity_type' => 'node',
    ':scraper_entity_type' => 'content_scraper_content',
  ));
  if ($entity_query->rowCount() >= 1) {
    while ($row = $entity_query->fetchAssoc()) {
      $link_ids[] = $row['link_id'];
    }
    $sids = content_scraper_link_fetch_sids_by_lids($link_ids);
  }

  if (!empty($sids)) {
    content_scraper_content_delete_multiple($sids);
  }

  // Delete link entity if no reference.
  if (!empty($link_ids) && is_array($link_ids)) {
    content_scraper_link_delete_multiple($link_ids);
  }

  // Delete reference record.
  db_delete('content_scraper_link_mapping')
    ->condition('entity_id', array($nid), 'IN')
    ->execute();
}
