<?php

/**
 * @file
 * Batch functions.
 */

/**
 * @param $nid
 * @param $context
 */
function _content_scraper_node_batch_fetch_links_process_entity($nid, &$context) {
  $node = node_load($nid, NULL, TRUE);
  // If node entity is valid.
  if ($node) {
    // Notify workbench.
    if (module_exists('workbench_moderation')) {
      $node->workbench_moderation['updating_live_revision'] = TRUE;
    }
    content_scraper_node_node_update($node);
  }

  // Update batch progress information.
  if (!isset($context['results']['processed'])) {
    $context['results']['processed'] = 0;
  }
  $context['results']['processed']++;
  $context['message'] = t('We are processing entity: @title with id @nid', array(
    '@title' => isset($node->title) ? $node->title : 'invalid node title',
    '@nid' => $nid,
  ));
}

/**
 * Batch process finished callback.
 * @param $success
 * @param $results
 * @param $operations
 */
function _content_scraper_node_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Bulk operations finished with @total entities - Content scraper.', array(
      '@total' => empty($results['processed']) ? 0 : $results['processed'],
    ));
    $severity = WATCHDOG_INFO;
    $type = 'status';
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ));
    $severity = WATCHDOG_ERROR;
    $type = 'error';
  }
  drupal_set_message($message, $type);
  // Add watchdog to log the operation status.
  watchdog('Content scraper', '%message', array('%message' => $message), $severity);
}
