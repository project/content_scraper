<?php

/**
 * @file
 * Definition of ScraperParserPlugin class.
 */

/**
 * Class ScraperParser
 */
abstract class ScraperParser extends ScraperPlugin {
  /**
   * @return string
   */
  public function pluginType() {
    return 'parser';
  }

  abstract public function parse(Scraper $source);

  /**
   * Strip away an element and its contents.
   *
   * @param string $html
   * The html to process.
   * @param string $element
   * The element name (eg img).
   * @param string $replacement
   * The replacement string.
   */
  protected function strip_element(&$html, $element, $replacement = '') {
    $pattern = '~<' . $element . '[\s\S]*?</' . $element . '>~';
    $html = preg_replace($pattern, $replacement, $html);
  }

  /**
   * Strip additional whitespace.
   *
   * Word HTML has an unfortunate tendency to insert extra linebreak mid tag.
   * This function resolves that issue.
   *
   * @param string $html
   * The HTML to be processed
   */
  protected function strip_spaces(&$html) {
    $pattern = '~[\s]+~';
    $replacement = ' ';
    $html = preg_replace($pattern, $replacement, $html);
  }

  /**
   * Remove any empty p or a elements.
   *
   * @param string $html
   * The HTML to be processed
   *
   * @TODO: make this configurable
   */
  protected function remove_empty_elements(&$html) {
    $pattern = '~<([p|a|i|div|li])[^>]*?>[\s]*<\/\1>~';
    $html = preg_replace($pattern, '', $html);
  }

  /**
   * @param $html
   * @param $xpathString
   * @return string
   */
  protected function removeDomNodes($html, $xpathString) {
    // Disable invalid HTML warning.
    libxml_use_internal_errors(TRUE);
    $dom = new DOMDocument;
    $dom->preserveWhiteSpace = FALSE;
    $dom->formatOutput = TRUE;
    $dom->loadHtml($html);
    $xpath = new DOMXPath($dom);
    while ($node = $xpath->query($xpathString)->item(0)) {
      $node->parentNode->removeChild($node);
    }

    return $dom->saveHTML();
  }

  /**
   * @param $htmlBlob
   * @return mixed
   */
  public static function removeHTMLTagsWithNoContent($htmlBlob) {
    $pattern = "/<[^\/>][^>]*><\/[^>]+>/";

    if (preg_match($pattern, $htmlBlob) == 1) {
      $htmlBlob = preg_replace($pattern, '', $htmlBlob);
      return self::removeHTMLTagsWithNoContent($htmlBlob);
    }
    else {
      return $htmlBlob;
    }
  }
}
