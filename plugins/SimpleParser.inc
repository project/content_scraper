<?php

/**
 * @file
 * Definition of SimpleParser class.
 */

/**
 * Class SimpleParser
 */
class SimpleParser extends ScraperParser {
  /**
   * @param \Scraper $source
   */
  public function parse(Scraper $source) {
    $raw = $source->getRemoteRaw();

    // Remove space and empty elements.
    $this->strip_spaces($raw);

    // UTF8 safe.
    if (method_exists('DOMDocument', 'loadHTML')) {
      // Remove tags.
      $removeHtmlTags = array(
        'script',
        'noscript',
        'iframe',
        'noframes',
        'embed',
        'noembed',
        'command',
        'canvas',
        'applet',
        'map',
        'menu',
        'svg',
        'style',
        'i',
        //'form',
        'input',
        'button',
        'link',
        'comment()',
      );
    }

    // remove script tags
    foreach ($removeHtmlTags as $tagId) {
      $raw = $this->removeDomNodes($raw, '//' . $tagId);
    }

    $this->removeHTMLTagsWithNoContent($raw);

    return $raw;
  }
}
