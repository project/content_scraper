<?php

/**
 * @file
 * Definition of ScraperFetcherPlugin class.
 */

/**
 * Base class for all fetcher results.
 */
class ScraperFetcherResult extends ScraperResult {
  protected $raw;
  protected $_url;
  protected $_code;
  protected $_message;

  /**
   * Constructor.
   */
  public function __construct($raw) {
    $this->raw = $raw;
  }

  /**
   * @return mixed
   */
  public function getUrl() {
    return $this->_url;
  }

  /**
   * @param $url
   */
  public function setUrl($url) {
    $this->_url = $url;
  }

  /**
   * @return mixed
   */
  public function getCode() {
    return $this->_code;
  }

  /**
   * @param mixed $code
   */
  public function setCode($code) {
    $this->_code = $code;
  }

  /**
   * @return mixed
   */
  public function getMessage() {
    return $this->_message;
  }

  /**
   * @param mixed $message
   */
  public function setMessage($message) {
    $this->_message = $message;
  }

  /**
   * @return
   *   The raw content from the source as a string.
   *
   * @throws Exception
   *   Extending classes MAY throw an exception if a problem occurred.
   */
  public function getRaw() {
    return $this->sanitizeRaw($this->raw);
  }

  /**
   * Sanitize the raw content string. Currently supported:
   *
   * - Remove BOM header from UTF-8 files.
   *
   * @param string $raw
   *   The raw content string to be sanitized.
   * @return
   *   The sanitized content as a string.
   */
  public function sanitizeRaw($raw) {
    if (substr($raw, 0, 3) == pack('CCC', 0xef, 0xbb, 0xbf)) {
      $raw = substr($raw, 3);
    }
    return $raw;
  }
}

/**
 * Class ScraperFetcher
 */
abstract class ScraperFetcher extends ScraperPlugin {
  /**
   * @return string
   */
  public function pluginType() {
    return 'fetcher';
  }

  /**
   * @param \Scraper $url
   * @return mixed
   */
  public abstract function fetch(Scraper $source);
}
