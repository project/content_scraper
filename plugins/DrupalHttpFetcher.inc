<?php

/**
 * @file
 * Definition of DrupalHttpFetcher class.
 */

/**
 * Class DrupalHttpFetcherResult
 */
class DrupalHttpFetcherResult extends ScraperFetcherResult {
  /**
   * Constructor.
   */
  public function __construct($url = NULL) {
    $this->_url = $url;
    if (!isset($this->raw)) {
      $result = drupal_http_request($this->_url);
      parent::setCode($result->code);
      parent::setMessage($result->status_message);
      if (isset($result->error)) {
        parent::setMessage($result->error);
      }
      $this->raw = !empty($result->data) ? $result->data : '';
    }
  }
}

/**
 * Class DrupalHttpFetcher
 */
class DrupalHttpFetcher extends ScraperFetcher {
  /**
   * @param \Scraper $source
   */
  public function fetch(Scraper $source) {
    $url = $source->getUrl();
    $fetcher_result = new DrupalHttpFetcherResult($url);
    return $fetcher_result;
  }
}
