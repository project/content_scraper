<?php

/**
 * @file
 * Definition of ScraperPlugin class.
 */

/**
 * Base class for a fetcher, parser or processor result.
 */
class ScraperResult {
}

/**
 * Class ScraperPlugin
 */
abstract class ScraperPlugin {
  /**
   * The plugin definition.
   *
   * @var array
   */
  protected $pluginDefinition;

  /**
   * Instantiates a ScraperPlugin object.
   *
   * Don't use directly, use content_scraper_plugin() instead.
   *
   * @see content_scraper_plugin()
   */
  public static function instance($class, array $plugin_definition = array()) {
    static $instances = array();

    if (!isset($instances[$class])) {
      $instance = new $class();

      // The ordering here is important. The plugin definition should be usable
      // in getConfig().
      $instance->setPluginDefinition($plugin_definition);
      $instances[$class] = $instance;
    }

    return $instances[$class];
  }

  /**
   * Returns the type of plugin.
   *
   * @return string
   *   One of either 'fetcher', 'parser', or 'processor'.
   */
  abstract public function pluginType();

  /**
   * Returns the plugin definition.
   *
   * @return array
   *   The plugin definition array.
   *
   * @see ctools_get_plugins()
   */
  public function pluginDefinition() {
    return $this->pluginDefinition;
  }

  /**
   * Sets the plugin definition.
   *
   * This is protected since we're only using it in ScraperPlugin::instance().
   *
   * @param array $plugin_definition
   *   The plugin definition.
   */
  protected function setPluginDefinition(array $plugin_definition) {
    $this->pluginDefinition = $plugin_definition;
  }

  /**
   * Get all available plugins.
   */
  public static function all() {
    ctools_include('plugins');
    $plugins = ctools_get_plugins('content_scraper', 'plugins');

    $result = array();
    foreach ($plugins as $key => $info) {
      if (!empty($info['hidden'])) {
        continue;
      }
      $result[$key] = $info;
    }

    return $result;
  }

  /**
   * Determines whether given plugin is derived from given base plugin.
   *
   * @param $plugin_key
   *   String that identifies a Feeds plugin key.
   * @param $parent_plugin
   *   String that identifies a Feeds plugin key to be tested against.
   *
   * @return
   *   TRUE if $parent_plugin is directly *or indirectly* a parent of $plugin,
   *   FALSE otherwise.
   */
  public static function child($plugin_key, $parent_plugin) {
    ctools_include('plugins');
    $plugins = ctools_get_plugins('content_scraper', 'plugins');
    $info = $plugins[$plugin_key];

    if (empty($info['handler']['parent'])) {
      return FALSE;
    }
    elseif ($info['handler']['parent'] == $parent_plugin) {
      return TRUE;
    }
    else {
      return self::child($info['handler']['parent'], $parent_plugin);
    }
  }

  /**
   * @param $plugin_key
   * @return bool|string
   */
  public static function type($plugin_key) {
    if (self::child($plugin_key, 'ScraperFetcher')) {
      return 'fetcher';
    }
    elseif (self::child($plugin_key, 'ScraperParser')) {
      return 'parser';
    }
    elseif (self::child($plugin_key, 'ScraperProcessor')) {
      return 'processor';
    }
    return FALSE;
  }

  /**
   * Gets all available plugins of a particular type.
   *
   * @param $type
   *   'fetcher', 'parser' or 'processor'
   */
  public static function byType($type) {
    $plugins = self::all();

    $result = array();
    foreach ($plugins as $key => $info) {
      if ($type == self::type($key)) {
        $result[$key] = $info;
      }
    }
    return $result;
  }
}
