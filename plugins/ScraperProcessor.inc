<?php

/**
 * @file
 * Definition of ScraperProcessorPlugin class.
 */

/**
 * Class ScraperProcessor
 */
abstract class ScraperProcessor extends ScraperPlugin {
  /**
   * @return string
   */
  public function pluginType() {
    return 'processor';
  }
}
