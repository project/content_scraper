<?php

/**
 * @file
 * Definition of Scraper class.
 */

/**
 * Class Scraper
 */
class Scraper {
  private $_url;
  private $_format;
  private $_fetcher;
  private $_parser;
  protected $remote_code;
  protected $remote_message;
  protected $remote_content;
  protected $remote_raw;

  /**
   * Constructor.
   */
  public function __construct($url = NULL, $fetcher = NULL, $parser = NULL) {
    $this->_url = $url;
    $this->_format = 0;
    $this->_fetcher = !empty($fetcher) ? $fetcher : 'DrupalHttpFetcher';
    $this->_parser = !empty($parser) ? $parser : 'SimpleParser';
  }

  /**
   * @return mixed
   */
  public function getUrl() {
    return $this->_url;
  }

  /**
   * @param mixed $url
   */
  public function setUrl($url) {
    $this->_url = $url;
  }

  /**
   * @return int
   */
  public function getFormat() {
    return $this->_format;
  }

  /**
   * @param int $format
   */
  public function setFormat($format) {
    $this->_format = $format;
  }

  /**
   * @return mixed
   */
  public function getFetcher() {
    return $this->_fetcher;
  }

  /**
   * @param mixed $fetcher
   */
  public function setFetcher($fetcher) {
    $this->_fetcher = $fetcher;
  }

  /**
   * @return mixed
   */
  public function getParser() {
    return $this->_parser;
  }

  /**
   * @param mixed $parser
   */
  public function setParser($parser) {
    $this->_parser = $parser;
  }

  /**
   * @return mixed
   */
  public function getRemoteCode() {
    return $this->remote_code;
  }

  /**
   * @param mixed $remote_code
   */
  public function setRemoteCode($remote_code) {
    $this->remote_code = $remote_code;
  }

  /**
   * @return mixed
   */
  public function getRemoteMessage() {
    return $this->remote_message;
  }

  /**
   * @param mixed $remote_message
   */
  public function setRemoteMessage($remote_message) {
    $this->remote_message = $remote_message;
  }

  /**
   * @return mixed
   */
  public function getRemoteRaw() {
    return $this->remote_raw;
  }

  /**
   * @param mixed $remote_raw
   */
  public function setRemoteRaw($remote_raw) {
    $this->remote_raw = $remote_raw;
  }

  /**
   * @return mixed
   */
  public function getRemoteContent() {
    $rawHtml = $this->remote_raw;
    $format = $this->_format;
    $this->remote_content = $this->getInnerHtml($rawHtml);
    if ($format == 1) {
      $this->remote_content = $this->getInnerText($this->remote_content);
    }

    return $this->remote_content;
  }

  /**
   * @param mixed $remote_content
   */
  public function setRemoteContent($remote_content) {
    $this->remote_content = $remote_content;
  }

  /**
   * Send request.
   */
  public function request() {
    // Fetcher.
    $fetcher = content_scraper_plugin($this->_fetcher);
    $fetch_object = $fetcher->fetch($this);
    $this->remote_code = $fetch_object->getCode();
    $this->remote_message = $fetch_object->getMessage();
    $this->remote_raw = $fetch_object->getRaw();
    // Parser.
    $parser = content_scraper_plugin($this->_parser);
    $this->remote_raw = $parser->parse($this);
  }

  /**
   * Return response object.
   */
  public function response() {
    $response = array(
      'code' => $this->remote_code,
      'message' => $this->remote_message,
      'raw' => $this->remote_raw,
      'content' => $this->getRemoteContent(),
    );
    return $response;
  }

  /**
   * Return Inner HTML.
   * @param $node
   * @return string
   */
  public static function getInnerHtml($rawHtml) {
    // Disable invalid HTML warning.
    libxml_use_internal_errors(TRUE);
    $doc = new DOMDocument;
    $doc->loadhtml($rawHtml);
    $body = $doc->getElementsByTagName('body');
    $node = $body->item(0);

    $innerHTML = '';
    $children = $node->childNodes;

    foreach ($children as $child) {
      $innerHTML .= $child->ownerDocument->saveXML($child);
    }

    return $innerHTML;
  }

  /**
   * Return Inner text.
   * @param $rawHtml
   */
  public static function getInnerText($rawHtml) {
    $plainText = '';
    // Disable invalid HTML warning.
    libxml_use_internal_errors(TRUE);
    $doc = new DOMDocument;
    $doc->preserveWhiteSpace = FALSE;
    if ($doc->loadhtml($rawHtml)) {
      $plainText = $doc->textContent;
    }

    // Remove white spaces.
    $pattern = '~[\s]+~';
    $replacement = ' ';
    $plainText = preg_replace($pattern, $replacement, $plainText);

    return $plainText;
  }
}
