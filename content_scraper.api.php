<?php

/**
 * @file
 * Hooks provided by the content scraper entity API.
 */

/**
 * Allows modules to deny or provide access for a user to perform a non-view
 * operation on an entity before any other access check occurs.
 *
 * Modules implementing this hook can return FALSE to provide a blanket
 * prevention for the user to perform the requested operation on the specified
 * entity. If no modules implementing this hook return FALSE but at least one
 * returns TRUE, then the operation will be allowed, even for a user without
 * role based permission to perform the operation.
 *
 * If no modules return FALSE but none return TRUE either, normal permission
 * based checking will apply.
 *
 * @param $op - the request operation: update, create, or delete.
 * @param $entity - The entity to perform the operation on.
 * @param $account - The user account whose access should be determined.
 * @param $entity_type -  The machine-name of the entity type of the given $entity.
 */
function hook_content_scraper_entity_access($op, $entity, $account, $entity_type) {
  // Placeholder.
}

/**
 * Lets modules specify the path information expected by a uri callback.
 *
 * @param $content
 * @return null
 *
 * @see content_scraper_content_uri()
 * @see entity_uri()
 */
function hook_content_scraper_content_uri($entity) {
  // If the content scraper entity has a display context, use it entity_uri().
  if (!empty($entity->display_context)) {
    return entity_uri($entity->display_context['entity_type'], $entity->display_context['entity']);
  }
}
