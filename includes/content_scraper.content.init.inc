<?php

/**
 * @file
 * Provides init functions.
 */

/**
 * Configure the scraping content types defined by enabled modules.
 */
function content_scraper_content_init_types() {
  foreach (content_scraper_content_types() as $type => $entity_type) {
    content_scraper_content_init_type($type);
  }
}

/**
 * Configures the fields on types provided by other modules.
 * @param null $modules
 */
function content_scraper_content_init_fields($modules = NULL) {
  // Load all modules implement types if no module in array.
  if (empty($modules)) {
    $modules = module_implements('content_scraper_content_type_info');
  }

  // Reset the scraping content type cache to get types added by newly enabled modules.
  content_scraper_content_types_reset();

  // Loop through all the enabled modules.
  foreach ($modules as $module) {
    // If the module implements hook_content_scraper_content_type_info()...
    if (module_hook($module, 'content_scraper_content_type_info')) {
      $entity_types = module_invoke($module, 'content_scraper_content_type_info');

      // Loop through and configure the product types defined by the module.
      foreach ($entity_types as $type => $entity_type) {
        content_scraper_content_init_type($type);
      }
    }
  }
}

/**
 * Ensures default settings and fields on a scraping content type bundle.
 */
function content_scraper_content_init_type($type) {
  content_scraper_content_create_instance('content_scraper', 'content_scraper_content', $type);
}

/**
 * Create a required, locked instance of a link and body field on the specified bundle.
 * @param $field_name
 * @param $entity_type
 * @param $bundle
 * @param $label
 * @param int $weight
 * @param array $display
 * @throws \Exception
 * @throws \FieldException
 * @see link_field_formatter_info()
 * @see text_field_formatter_info()
 */
function content_scraper_content_create_instance($field_namespace = 'content_scraper', $entity_type, $bundle, $display = array()) {
  module_load_include('inc', 'content_scraper', 'includes/content_scraper.helper');

  // Look for or add the specified field to the requested entity bundle.
  $field_link_name = $field_namespace . '_url';
  $field_body_name = $field_namespace . '_body';

  // Check if fields are already active.
  content_scraper_activate_field($field_link_name);
  content_scraper_activate_field($field_body_name);
  field_info_cache_clear();

  $field_link = field_info_field($field_link_name);
  $field_link_instance = field_info_instance($entity_type, $field_link_name, $bundle);

  if (empty($field_link)) {
    // Create link field.
    $field_link = array(
      'field_name' => $field_link_name,
      'type' => 'link_field',
      'cardinality' => 1,
      'entity_types' => array($entity_type),
      'translatable' => FALSE,
      'locked' => TRUE,
    );
    field_create_field($field_link);
  }

  if (empty($field_link_instance)) {
    $field_link_instance = array(
      'field_name' => $field_link_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => t('URL'),
      'required' => TRUE,
      'settings' => array(),
      'widget' => array(
        'type' => 'link_field',
        'weight' => 1,
        'settings' => array(),
      ),
      'display' => array(),
    );

    $entity_info = entity_get_info($entity_type);

    // Spoof the default view mode and node teaser so its display type is set.
    $entity_info['view modes'] += array(
      'default' => array(),
    );

    foreach ($entity_info['view modes'] as $view_mode => $data) {
      $field_link_instance['display'][$view_mode] = $display + array(
          'label' => 'hidden',
          'type' => 'link_default',
          'settings' => array(),
          'weight' => 1,
        );
    }

    field_create_instance($field_link_instance);
  }

  $field_body = field_info_field($field_body_name);
  $field_body_instance = field_info_instance($entity_type, $field_body_name, $bundle);

  if (empty($field_body)) {
    // Create a text with summary field for scraping content.
    $field_body = array(
      'field_name' => $field_body_name,
      'type' => 'text_with_summary',
      'cardinality' => 1,
      'entity_types' => array($entity_type),
      'translatable' => FALSE,
      'locked' => TRUE,
    );
    field_create_field($field_body);
  }

  if (empty($field_body_instance)) {
    $field_body_instance = array(
      'field_name' => $field_body_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => t('Scraped content'),
      'required' => FALSE,
      'settings' => array(),
      'widget' => array(
        'type' => 'text_textarea_with_summary',
        'weight' => 99,
        'settings' => array(),
      ),
      'display' => array(),
    );

    $entity_info = entity_get_info($entity_type);

    // Spoof the default view mode and node teaser so its display type is set.
    $entity_info['view modes'] += array(
      'default' => array(),
    );

    foreach ($entity_info['view modes'] as $view_mode => $data) {
      $field_body_instance['display'][$view_mode] = $display + array(
          'label' => 'hidden',
          'type' => 'text_default',
          'settings' => array(),
          'weight' => 99,
        );
    }

    field_create_instance($field_body_instance);
  }
}
