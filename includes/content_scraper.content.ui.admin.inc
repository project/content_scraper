<?php

/**
 * @file
 * Admin file for the module.
 */

/**
 * Provides admin form for content scraper content type.
 * @param $form
 * @param $form_state
 * @param $type
 * @return mixed
 */
function content_scraper_content_type_form($form, &$form_state, $entity, $op = 'edit', $entity_type = NULL) {
  // Load module inc file.
  module_load_include('inc', 'content_scraper', 'includes/content_scraper.func');

  // Make sure form is available.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'content_scraper') . '/includes/content_scraper.content.ui.admin.inc';

  if ($op == 'clone') {
    // Only label is provided for cloned entities.
    $entity->label .= ' (cloned)';
    $entity->name = $entity_type . '_clone';
  }

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#description' => t('The human-readable name of this content type. This text will be displayed as part of the list on the Add new content page. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#default_value' => $entity->label,
    '#maxlength' => 255,
    '#weight' => -10,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Type'),
    '#required' => TRUE,
    '#default_value' => $entity->name,
    '#maxlength' => 255,
    '#weight' => -9,
    '#machine_name' => array(
      'exists' => 'content_scraper_content_type_name_exists',
      'source' => array('label'),
      'label' => t('Machine name'),
    ),
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
  );

  $form['settings']['format'] = array(
    '#type' => 'checkbox',
    '#title' => t('Plain text'),
    '#default_value' => $entity->data['format'],
    '#description' => t('Plain text can be used for search boost with Search API or ApacheSolr module'),
  );

  $form['settings']['revision'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create new revision'),
    '#default_value' => $entity->data['revision'],
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 400,
  );

  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save content type'),
    '#submit' => array_merge($submit, array('content_scraper_content_type_form_submit')),
  );

  return $form;
}

/**
 * Submit callback.
 * @param $form
 * @param $form_state
 */
function content_scraper_content_type_form_submit($form, &$form_state) {
  $entity = &$form_state['content_scraper_content_type'];

  // Save default parameters back into the $entity object.
  $entity->label = $form_state['values']['label'];
  $entity->name = $form_state['values']['name'];
  $entity->data['format'] = $form_state['values']['format'];
  $entity->data['revision'] = $form_state['values']['revision'];

  // Save content type.
  content_scraper_content_type_save($entity);

  // Redirect based on the button clicked.
  $form_state['redirect'] = url('admin/content/content-scraper/type');
  drupal_set_message(t('Content scraper content type saved.'));
}

/**
 * Implements hook_form().
 * @param $form
 * @param $form_state
 * @param null $entity
 */
function content_scraper_content_form($form, &$form_state, $entity) {
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'content_scraper') . '/includes/content_scraper.content.ui.admin.inc';

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $entity->label,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  // Add the field related form elements.
  $form_state['content_scraper_content'] = $entity;

  $langcode = entity_language('content_scraper_content', $entity);

  if (empty($langcode)) {
    $langcode = LANGUAGE_NONE;
  }

  field_attach_form('content_scraper_content', $entity, $form, $form_state, $langcode);

  $form['status'] = array(
    '#type' => 'radios',
    '#title' => t('Status'),
    '#description' => t('Status.'),
    '#options' => array(
      '1' => t('Active'),
      '0' => t('Disabled'),
    ),
    '#default_value' => $entity->status,
    '#required' => TRUE,
    '#weight' => 100,
  );

  $content_type = content_scraper_content_type_load($entity->type);

  if ($content_type->data['revision'] == 1) {
    $form['change_history'] = array(
      '#type' => 'fieldset',
      '#title' => t('Change history'),
      '#collapsible' => TRUE,
      '#collapsed' => empty($entity->sid) || empty($content_type->data['revision']),
      '#weight' => 350,
    );
    if (!empty($entity->sid)) {
      $form['change_history']['revision'] = array(
        '#type' => 'checkbox',
        '#title' => t('Create new revision on update'),
        '#description' => t('If an update log message is entered, a revision will be created even if this is unchecked.'),
        '#default_value' => $content_type->data['revision'],
        '#access' => user_access('administer content_scraper_content entities'),
      );
    }
    $form['change_history']['log'] = array(
      '#type' => 'textarea',
      '#title' => !empty($entity->sid) ? t('Update log message') : t('Creation log message'),
      '#rows' => 4,
      '#description' => t('Provide an explanation of the changes you are making. This will provide a meaningful history of changes to this content.'),
    );
  }

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 400,
  );

  // Simply use default language
  $form['language'] = array(
    '#type' => 'value',
    '#value' => $langcode,
  );

  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save entity'),
    '#submit' => array_merge($submit, array('content_scraper_content_form_submit')),
  );

  return $form;
}

/**
 * Submit callback.
 * @param $form
 * @param $form_state
 */
function content_scraper_content_form_submit($form, &$form_state) {
  global $user;

  $entity = &$form_state['content_scraper_content'];

  // Save default parameters back into the $entity object.
  $entity->label = $form_state['values']['title'];
  $entity->status = $form_state['values']['status'];
  $entity->language = $form_state['values']['language'];

  // Set the uid if it's being created at this time.
  if (empty($entity->sid)) {
    $entity->uid = $user->uid;
  }

  // Trigger a new revision if the checkbox was enabled or a log message supplied.
  if ((user_access('administer content_scraper_content entities') && !empty($form_state['values']['revision'])) ||
    (!user_access('administer content_scraper_content entities') && !empty($form['change_history']['revision']['#default_value'])) ||
    !empty($form_state['values']['log'])
  ) {
    $entity->revision = TRUE;
    $entity->log = $form_state['values']['log'];
  }

  // Notify field widgets.
  field_attach_submit('content_scraper_content', $entity, $form, $form_state);

  // Save the product.
  content_scraper_content_save($entity);

  // Redirect based on the button clicked.
  $form_state['redirect'] = url('admin/content/content-scraper/content');
  drupal_set_message(t('Content scraper content entity saved.'));
}
