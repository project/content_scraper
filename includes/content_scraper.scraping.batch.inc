<?php

/**
 * @file
 * Batch functions.
 */

/**
 * @param $sid
 * @param $context
 */
function _content_scraper_scraping_bulk_update_batch_process_entity($sid, &$context) {
  $cs_entity = content_scraper_content_load($sid);
  if ($cs_entity) {
    module_load_include('inc', 'content_scraper', 'includes/content_scraper.func');
    content_scraper_scraping_update_entity($cs_entity);
  }

  // Update batch progress information.
  if (!isset($context['results']['processed'])) {
    $context['results']['processed'] = 0;
  }
  $context['results']['processed']++;
  $context['message'] = t('We are processing entity: @title', array(
    '@title' => isset($cs_entity->label) ? $cs_entity->label : 'invalid entity label',
  ));

  // Set 1 second break for memory and timeout limit.
  sleep(1);
}

/**
 * Batch process finished callback.
 * @param $success
 * @param $results
 * @param $operations
 */
function _content_scraper_scraping_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('Bulk operations finished with @total entities - Content scraper.', array(
      '@total' => empty($results['processed']) ? 0 : $results['processed'],
    ));
    $severity = WATCHDOG_INFO;
    $type = 'status';
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ));
    $severity = WATCHDOG_ERROR;
    $type = 'error';
  }
  drupal_set_message($message, $type);
  // Add watchdog to log the operation status.
  watchdog('Content scraper', '%message', array('%message' => $message), $severity);
}
