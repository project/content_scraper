<?php

/**
 * @file
 * Provides helper function.
 */

function content_scraper_entity_access_permissions($entity_type) {
  $entity_info = entity_get_info($entity_type);
  $labels = $entity_info['permission labels'];

  $permissions = array();

  // General 'administer' permission.
  $permissions['administer ' . $entity_type . ' entities'] = array(
    'title' => t('Administer @entity_type', array('@entity_type' => $labels['plural'])),
    'description' => t('Allows users to perform any action on @entity_type.', array('@entity_type' => $labels['plural'])),
    'restrict access' => TRUE,
  );

  // Generic create and edit permissions.
  $permissions['create ' . $entity_type . ' entities'] = array(
    'title' => t('Create @entity_type of any type', array('@entity_type' => $labels['plural'])),
  );
  if (!empty($entity_info['access arguments']['user key'])) {
    $permissions['edit own ' . $entity_type . ' entities'] = array(
      'title' => t('Edit own @entity_type of any type', array('@entity_type' => $labels['plural'])),
    );
  }
  $permissions['edit any ' . $entity_type . ' entity'] = array(
    'title' => t('Edit any @entity_type of any type', array('@entity_type' => $labels['singular'])),
    'restrict access' => TRUE,
  );
  if (!empty($entity_info['access arguments']['user key'])) {
    $permissions['view own ' . $entity_type . ' entities'] = array(
      'title' => t('View own @entity_type of any type', array('@entity_type' => $labels['plural'])),
    );
  }
  $permissions['view any ' . $entity_type . ' entity'] = array(
    'title' => t('View any @entity_type of any type', array('@entity_type' => $labels['singular'])),
    'restrict access' => TRUE,
  );

  // Per-bundle create and edit permissions.
  if (!empty($entity_info['entity keys']['bundle'])) {
    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      $permissions['create ' . $entity_type . ' entities of bundle ' . $bundle_name] = array(
        'title' => t('Create %bundle @entity_type', array(
          '@entity_type' => $labels['plural'],
          '%bundle' => $bundle_info['label']
        )),
      );
      if (!empty($entity_info['access arguments']['user key'])) {
        $permissions['edit own ' . $entity_type . ' entities of bundle ' . $bundle_name] = array(
          'title' => t('Edit own %bundle @entity_type', array(
            '@entity_type' => $labels['plural'],
            '%bundle' => $bundle_info['label']
          )),
        );
      }
      $permissions['edit any ' . $entity_type . ' entity of bundle ' . $bundle_name] = array(
        'title' => t('Edit any %bundle @entity_type', array(
          '@entity_type' => $labels['singular'],
          '%bundle' => $bundle_info['label']
        )),
        'restrict access' => TRUE,
      );
      if (!empty($entity_info['access arguments']['user key'])) {
        $permissions['view own ' . $entity_type . ' entities of bundle ' . $bundle_name] = array(
          'title' => t('View own %bundle @entity_type', array(
            '@entity_type' => $labels['plural'],
            '%bundle' => $bundle_info['label']
          )),
        );
      }
      $permissions['view any ' . $entity_type . ' entity of bundle ' . $bundle_name] = array(
        'title' => t('View any %bundle @entity_type', array(
          '@entity_type' => $labels['singular'],
          '%bundle' => $bundle_info['label']
        )),
        'restrict access' => TRUE,
      );
    }
  }

  return $permissions;
}

/**
 * Return class with http code.
 * @param $http_code
 * @return string
 */
function content_scraper_content_admin_ui_code($http_code = 0) {
  switch ($http_code) {
    case '0':
      $class = 'error';
      break;
    case '-60':
      $class = 'error';
      break;
    case '-110':
      $class = 'error';
      break;
    case '-111':
      $class = 'error';
      break;
    case '-1002':
      $class = 'error';
      break;
    case '200':
      $class = 'ok';
      break;
    case '301':
      $class = 'warning';
      break;
    case '302':
      $class = 'warning';
      break;
    case '303':
      $class = 'warning';
      break;
    case '403':
      $class = 'error';
      break;
    case '404':
      $class = 'error';
      break;
    case '500':
      $class = 'error';
      break;
    case '503':
      $class = 'error';
      break;
    default:
      $class = 'info';
  }
  return $class;
}

/**
 * Check weather content scraper content type name exists.
 * @see content_scraper_content_type_form()
 */
function content_scraper_content_type_name_exists($name) {
  if (content_scraper_content_type_load($name)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Update link and content entity.
 * @param $cs_entity
 */
function content_scraper_scraping_update_entity($cs_entity) {
  // Get link target in current entity.
  $links = field_get_items('content_scraper_content', $cs_entity, 'content_scraper_url');
  // Return if link is empty or not valid.
  if (!$links || empty($links[0]['url']) || !valid_url($links[0]['url'], $absolute = TRUE)) {
    return FALSE;
  }
  // Get current entity link.
  $link = $links[0];

  // Scraping content and get response.
  $cs_entity_type = content_scraper_content_type_load($cs_entity->type);
  if ($cs_entity_type && $cs_entity_type->data['format'] == 1) {
    $format = 1;
  } else {
    $format = 0;
  }
  $scraped_content = content_scraper_scraping($link, $format);

  // Update link entity.
  // Load module inc file.
  module_load_include('inc', 'content_scraper', 'includes/content_scraper.handler');
  $link_id = content_scraper_content_save_link($link, $cs_entity);
  if ($link_id) {
    $cs_entity_link = content_scraper_link_load($link_id);
    $cs_entity_link->code = $scraped_content['code'];
    $cs_entity_link->message = $scraped_content['message'];
    $cs_entity_link->checked_time = REQUEST_TIME;
    if (variable_get('content_scraper_scraping_crawl_save', FALSE)) {
      $cs_entity_link->data = serialize($scraped_content);
    }
    else {
      $cs_entity_link->data = serialize(array());
    }
    content_scraper_link_save($cs_entity_link);
  }

  if (!empty($scraped_content['content'])) {
    $remote_content = $scraped_content['content'];

    /**
     * Attach link to entity.
     *
     * @TODO security solutions on imported HTML.
     *
     * @see check_plain()
     * @see drupal_validate_utf8()
     */
    $cs_entity->content_scraper_body[LANGUAGE_NONE][0]['value'] = $remote_content;
    field_attach_update('content_scraper_content', $cs_entity);
  }
}

/**
 * Return link_ids from link.
 * @param $sids
 */
function content_scraper_scraping_fetch_cron_links() {
  $interval = variable_get('content_scraper_scraping_crawl_interval', 7 * 24 * 60 * 60);
  $limit = variable_get('content_scraper_scraping_cron_limit', 20);
  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'content_scraper_link');
  $query->propertyCondition('checked_time', REQUEST_TIME - $interval, '<=');
  $query->addMetaData('account', user_load(1));
  $query->range(0, $limit);
  $result = $query->execute();
  if (isset($result['content_scraper_link'])) {
    $lids = array_keys($result['content_scraper_link']);
    // Load module inc file.
    module_load_include('inc', 'content_scraper', 'includes/content_scraper.handler');
    $sids = content_scraper_link_fetch_sids_by_lids($lids);
    return $sids;
  }
  else {
    return array();
  }
}

/**
 * Convert special ascii codes.
 * @param $string
 * @return mixed
 */
function content_scraper_scraping_convert_ascii($string) {
  // Replace Single Curly Quotes
  $search[] = chr(226) . chr(128) . chr(152);
  $replace[] = "'";
  $search[] = chr(226) . chr(128) . chr(153);
  $replace[] = "'";

  // Replace Smart Double Curly Quotes
  $search[] = chr(226) . chr(128) . chr(156);
  $replace[] = '"';
  $search[] = chr(226) . chr(128) . chr(157);
  $replace[] = '"';

  // Replace En Dash
  $search[] = chr(226) . chr(128) . chr(147);
  $replace[] = '--';

  // Replace Em Dash
  $search[] = chr(226) . chr(128) . chr(148);
  $replace[] = '---';

  // Replace Bullet
  $search[] = chr(226) . chr(128) . chr(162);
  $replace[] = '*';

  // Replace Middle Dot
  $search[] = chr(194) . chr(183);
  $replace[] = '*';

  // Replace Ellipsis with three consecutive dots
  $search[] = chr(226) . chr(128) . chr(166);
  $replace[] = '...';

  // Apply Replacements
  $string = str_replace($search, $replace, $string);

  return $string;
}

/**
 * @param $text
 * @return mixed|string
 */
function content_scraper_scraping_clean_text($text) {
  // Replace new line.
  $newline_chars = array(chr(13), "\r\n", '\r', '\n', '&nbsp;');
  $text = str_replace($newline_chars, " ", $text);

  // Convert special ascii codes.
  $text = content_scraper_scraping_convert_ascii($text);

  // Remove invisible content.
  $text = preg_replace('@<(applet|audio|canvas|command|embed|iframe|map|menu|noembed|noframes|noscript|script|style|svg|video)[^>]*>.*</\1>@siU', ' ', $text);
  // Add spaces before stripping tags to avoid running words together.
  $text = filter_xss(str_replace(array('<', '>'), array(
    ' <',
    '> '
  ), $text), array());
  // Decode entities and then make safe any < or > characters.
  $text = htmlspecialchars(html_entity_decode($text, ENT_QUOTES, 'UTF-8'), ENT_QUOTES, 'UTF-8');
  // Remove extra spaces.
  $text = preg_replace('/\s+/s', ' ', $text);
  // Remove white spaces around punctuation marks probably added
  // by the safety operations above. This is not a world wide perfect solution,
  // but a rough attempt for at least US and Western Europe.
  // Pc: Connector punctuation
  // Pd: Dash punctuation
  // Pe: Close punctuation
  // Pf: Final punctuation
  // Pi: Initial punctuation
  // Po: Other punctuation, including ¿?¡!,.:;
  // Ps: Open punctuation
  $text = preg_replace('/\s(\p{Pc}|\p{Pd}|\p{Pe}|\p{Pf}|!|\?|,|\.|:|;)/s', '$1', $text);
  $text = preg_replace('/(\p{Ps}|¿|¡)\s/s', '$1', $text);
  return $text;
}
