<?php

/**
 * @file
 * Provides helper function.
 */

/**
 * Save link entity with content scraper content save.
 * @see ContentScraperContentEntityController
 */
function content_scraper_content_save_link($link, $entity) {
  if (empty($link['url'])) {
    return FALSE;
  }
  $extras = array(
    'entity_id' => empty($entity->sid) ? NULL : $entity->sid,
    'entity_type' => 'content_scraper_content',
    'entity_bundle' => empty($entity->type) ? NULL : $entity->type,
  );
  return content_scraper_link_save_link($link, $extras);
}

/**
 * Delete link from given entity ids.
 * @param $entity_ids
 * @return bool
 */
function content_scraper_content_delete_link($entity_ids) {
  if (empty($entity_ids)) {
    return FALSE;
  }
  $link_ids = array();
  foreach ($entity_ids as $entity_id) {
    // Delete content scraper entity.
    $entity_query = db_query('SELECT `cslm`.`link_id` FROM `content_scraper_link_mapping` cslm WHERE `cslm`.`link_id` IN (SELECT `link_id` FROM `content_scraper_link_mapping` WHERE `entity_id` = :entity_id AND `entity_type` = :entity_type) GROUP BY `cslm`.`link_id` HAVING (count(`cslm`.`entity_id`) = 1)', array(
      ':entity_id' => $entity_id,
      ':entity_type' => 'content_scraper_content'
    ));
    if ($entity_query->rowCount() >= 1) {
      while ($row = $entity_query->fetchAssoc()) {
        $link_ids[] = $row['link_id'];
      }
    }
  }

  // Delete link entity if no reference.
  content_scraper_link_delete_multiple($link_ids);

  // Delete reference record.
  db_delete('content_scraper_link_mapping')
    ->condition('entity_id', $entity_ids, 'IN')
    ->execute();
}

/**
 * Fetch all sids.
 */
function content_scraper_content_fetch_sids() {
  $sids = &drupal_static(__FUNCTION__);
  if (!isset($sids)) {
    $sids = array();
    $result = db_select('content_scraper_content', 'cs')
      ->fields('cs', array('sid', 'type', 'status'))
      ->condition('status', '1')
      ->execute();

    if ($result->rowCount() > 0) {
      $sids = $result->fetchCol();
    }
  }
  return $sids;
}

/**
 * Save link entity.
 * @param $link
 * @param array $extras
 * @return bool
 * @throws \Exception
 * @throws \InvalidMergeQueryException
 */
function content_scraper_link_save_link($link, $extras = array()) {
  // Check URL.
  if (empty($link) || empty($link['url']) || !valid_url($link['url'], TRUE)) {
    return FALSE;
  }

  $link_id = content_scraper_link_fetch_lid_by_link($link['url']);
  $link_url = $link['url'];
  $link_title = !empty($link['title']) ? $link['title'] : check_plain($link['url']);

  if (!$link_id) {
    // Create link entity.
    $link_type = url_is_external($link_url) === TRUE ? 'external' : 'internal';
    $cs_entity_link = content_scraper_link_create($link_type);
    $cs_entity_link->label = $link_title;
    $cs_entity_link->language = LANGUAGE_NONE;
    $cs_entity_link->hash = drupal_hash_base64($link_url);
    $cs_entity_link->uri = $link_url;
    $cs_entity_link_wrapper = entity_metadata_wrapper('content_scraper_link', $cs_entity_link);
    $cs_entity_link_wrapper->save();
    $link_id = $cs_entity_link_wrapper->link_id->value();
  }

  if ($link_id && !empty($extras['entity_id']) && !empty($extras['entity_type'])) {
    db_merge('content_scraper_link_mapping')
      ->key(array(
        'link_id' => $link_id,
        'entity_id' => $extras['entity_id'],
        'entity_type' => $extras['entity_type']
      ))
      ->fields(array(
        'link_id' => $link_id,
        'entity_id' => $extras['entity_id'],
        'entity_type' => !empty($extras['entity_type']) ? $extras['entity_type'] : 'content_scraper_content',
        'entity_bundle' => !empty($extras['entity_bundle']) ? $extras['entity_bundle'] : 'default',
        'status' => isset($extras['status']) ? $extras['status'] : CONTENT_SCRAPER_NODE_ACTIVE,
      ))
      ->execute();
  }

  return $link_id;
}

/**
 * Return sid by link.
 * @param $link
 * @return bool
 */
function content_scraper_link_fetch_sid_by_link($link) {
  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'content_scraper_link');
  $query->propertyCondition('hash', drupal_hash_base64($link), '=');
  $query->addMetaData('account', user_load(1));
  $result = $query->execute();
  if (isset($result['content_scraper_link'])) {
    $link_ids = array_keys($result['content_scraper_link']);
  }
  if (isset($link_ids[0])) {
    $link_entity = content_scraper_link_load($link_ids[0]);
    return $link_entity->sid;
  }
  else {
    return FALSE;
  }
}

/**
 * Return link_id from link.
 * @param $link
 */
function content_scraper_link_fetch_lid_by_link($link) {
  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'content_scraper_link');
  $query->propertyCondition('hash', drupal_hash_base64($link), '=');
  $query->addMetaData('account', user_load(1));
  $result = $query->execute();
  if (isset($result['content_scraper_link'])) {
    $link_ids = array_keys($result['content_scraper_link']);
  }
  if (isset($link_ids[0])) {
    return $link_ids[0];
  }
  else {
    return FALSE;
  }
}

/**
 * Return link_id from link.
 * @param $entity_id
 * @param null $entity_type
 * @param null $entity_bundle
 */
function content_scraper_link_fetch_lids_by_entity_id($entity_id, $entity_type = NULL, $entity_bundle = NULL) {
  $link_ids = content_scraper_link_fetch_lids_by_entity_ids(array($entity_id), $entity_type, $entity_bundle);
  return $link_ids;
}

function content_scraper_link_fetch_lids_by_entity_ids($entity_ids, $entity_type = NULL, $entity_bundle = NULL) {
  $link_ids = array();
  if (empty($entity_bundle)) {
    $result = db_query('SELECT link_id FROM {content_scraper_link_mapping} WHERE entity_id IN (:entity_ids) AND entity_type = :entity_type', array(
      ':entity_ids' => $entity_ids,
      ':entity_type' => empty($entity_type) ? 'entity_type' : $entity_type,
    ));
  }
  else {
    $result = db_query('SELECT link_id FROM {content_scraper_link_mapping} WHERE entity_id IN (:entity_ids) AND entity_type = :entity_type AND entity_bundle = :entity_bundle', array(
      ':entity_ids' => $entity_ids,
      ':entity_type' => empty($entity_type) ? 'entity_type' : $entity_type,
      ':entity_bundle' => $entity_bundle,
    ));
  }

  if ($result) {
    while ($row = $result->fetchAssoc()) {
      $link_ids[] = $row['link_id'];
    }
  }

  return $link_ids;
}

/**
 * Return sids by given link ids.
 * @param array $link_ids
 * @return array
 */
function content_scraper_link_fetch_sids_by_lids($link_ids = array()) {
  $sids = array();
  if (empty($link_ids)) {
    return array();
  }
  $query_sid = db_query('SELECT entity_id FROM {content_scraper_link_mapping} WHERE link_id IN (:link_ids) AND entity_type = :scraper_entity_type', array(
    ':link_ids' => $link_ids,
    ':scraper_entity_type' => 'content_scraper_content',
  ));
  if ($query_sid->rowCount() >= 1) {
    while ($row = $query_sid->fetchAssoc()) {
      $sids[] = $row['entity_id'];
    }
  }
  return $sids;
}

/**
 * @param $entity_id
 * @param null $entity_type
 * @return array
 */
function content_scraper_link_fetch_sids_by_entity_id($entity_id, $entity_type = NULL) {
  $sids = content_scraper_link_fetch_sids_by_entity_ids(array($entity_id), $entity_type);
  return $sids;
}

/**
 * @param array $entity_ids
 * @param null $entity_type
 * @return array
 */
function content_scraper_link_fetch_sids_by_entity_ids($entity_ids = array(), $entity_type = NULL) {
  $sids = array();

  if (empty($entity_ids) || empty($entity_type)) {
    return array();
  }

  $link_ids = content_scraper_link_fetch_lids_by_entity_ids($entity_ids, $entity_type);
  if (!empty($link_ids)) {
    if (sizeof($link_ids) == 1) {
      $query_sid = db_query('SELECT entity_id FROM {content_scraper_link_mapping} WHERE link_id = (:link_id) AND entity_type = :entity_type', array(
        ':link_id' => $link_ids[0],
        ':entity_type' => 'content_scraper_content',
      ));
    }
    else {
      $query_sid = db_query('SELECT entity_id FROM {content_scraper_link_mapping} WHERE link_id IN (:link_ids) AND entity_type = :entity_type', array(
        ':link_ids' => $link_ids,
        ':entity_type' => 'content_scraper_content',
      ));
    }
    if ($query_sid->rowCount() >= 1) {
      while ($row = $query_sid->fetchAssoc()) {
        $sids[] = $row['entity_id'];
      }
    }
  }

  return $sids;
}
