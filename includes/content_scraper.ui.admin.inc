<?php

/**
 * @file
 * Admin file for the module.
 */

/**
 * Generates the type editing form.
 */
function content_scraper_type_form($form, &$form_state, $entity, $op = 'edit', $entity_type = NULL) {
  if ($op == 'clone') {
    $entity->label .= ' (cloned)';
    $entity->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->label) ? $entity->label : '',
    '#description' => t('The human-readable name of this type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($entity->type) ? $entity->type : '',
    '#maxlength' => 32,
    '#disabled' => $entity->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'content_scraper_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['data']['#tree'] = TRUE;
  $form['data']['plain'] = array(
    '#type' => 'checkbox',
    '#title' => t('Plain text.'),
    '#default_value' => !empty($entity->data['plain']),
  );

  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save content scraper type'),
    '#weight' => 40,
  );

  if (!$entity->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
    );
  }

  return $form;
}

/**
 * Submit handler of content scraper types form.
 * @param $form
 * @param $form_state
 */
function entity_content_scraper_type_form_submit($form, &$form_state) {
  $type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $type->save();
  $form_state['redirect'] = 'admin/structure/content-scraper';
}

/**
 * Overview form.
 * @param $form
 * @param $form_state
 * @return array
 */
function content_scraper_overview($form, &$form_state) {
  $form = array();
  $form['content_scraper_overview'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => 'overview',
  );
  // Crawl settings.
  $form['content_scraper_overview']['crawl'] = array(
    '#type' => 'fieldset',
    '#weight' => 8,
    '#title' => t('Crawl setting'),
    '#group' => 'content_scraper_overview',
  );
  $form['content_scraper_overview']['crawl']['content_scraper_scraping_crawl_interval'] = array(
    '#type' => 'item',
    '#title' => t('Crawl interval (seconds)'),
    '#description' => t('Fetch content after a certain time. default setting is 7 days'),
    '#markup' => variable_get('content_scraper_scraping_crawl_interval', 7 * 24 * 60 * 60),
  );
  // Cron job.
  $form['content_scraper_overview']['cron'] = array(
    '#type' => 'fieldset',
    '#weight' => 8,
    '#title' => t('Cron job'),
    '#group' => 'content_scraper_overview',
  );
  $form['content_scraper_overview']['cron']['content_scraper_scraping_cron_enabled'] = array(
    '#type' => 'item',
    '#title' => t('Cron job:'),
    '#markup' => variable_get('content_scraper_scraping_cron_enabled', 'FALSE') === 1 ? 'Enabled' : 'Disabled',
  );
  $form['content_scraper_overview']['cron']['content_scraper_scraping_cron_interval'] = array(
    '#type' => 'item',
    '#title' => t('Cron interval (seconds)'),
    '#markup' => variable_get('content_scraper_scraping_cron_interval', 1 * 60 * 60),
  );
  $form['content_scraper_overview']['cron']['last_run'] = array(
    '#type' => 'item',
    '#title' => t('Cron last run:'),
    '#markup' => format_date(variable_get('content_scraper_scraping_cron_last_execution', 0), 'custom', 'D, m/d/Y - H:i'),
  );
  $form['content_scraper_overview']['cron']['next_run'] = array(
    '#type' => 'item',
    '#title' => t('Cron next run:'),
    '#markup' => format_date(variable_get('content_scraper_scraping_cron_next_execution', 0), 'custom', 'D, m/d/Y - H:i'),
  );

  return $form;
}

/**
 * Settings config form.
 * @param $form
 * @param $form_state
 */
function content_scraper_config($form, &$form_state) {
  $form = array();
  $form['content_scraper_config'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => 'overview',
  );
  $form['content_scraper_config']['search'] = array(
    '#type' => 'fieldset',
    '#weight' => 8,
    '#title' => t('Search integration'),
    '#group' => 'content_scraper_config',
  );

  // Get plugins and configuration.
  // $plugins = ScraperPlugin::all();
  $plugins_fetcher_options = array();
  $plugins_parser_options = array();
  $plugins_fetcher = ScraperPlugin::byType('fetcher');
  foreach ($plugins_fetcher as $key => $plugin) {
    $plugins_fetcher_options[$key] = $plugin['name'];
  }
  $plugins_parser = ScraperPlugin::byType('parser');
  foreach ($plugins_parser as $key => $plugin) {
    $plugins_parser_options[$key] = $plugin['name'];
  }

  // Crawl settings.
  $form['content_scraper_config']['crawl'] = array(
    '#type' => 'fieldset',
    '#weight' => 8,
    '#title' => t('Crawl setting'),
    '#group' => 'content_scraper_config',
  );
  $form['content_scraper_config']['crawl']['content_scraper_scraping_crawl_interval'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 10,
    '#title' => t('Crawl interval (seconds)'),
    '#default_value' => variable_get('content_scraper_scraping_crawl_interval', 7 * 24 * 60 * 60),
  );
  $form['content_scraper_config']['crawl']['content_scraper_scraping_crawl_save'] = array(
    '#type' => 'checkbox',
    '#title' => t('Save original HTML in database'),
    '#default_value' => variable_get('content_scraper_scraping_crawl_save', FALSE),
    '#description' => t('Due to the size of original HTML (usually 20-200KB), please be carefully to enable this setting as 1000 unique links may use up to two hundreds of MB in database'),
  );
  $form['content_scraper_config']['crawl']['content_scraper_scraping_crawl_fetcher'] = array(
    '#type' => 'select',
    '#title' => t('Crawl fetcher'),
    '#options' => $plugins_fetcher_options,
    '#default_value' => variable_get('content_scraper_scraping_crawl_fetcher', 'DrupalHttpFetcher'),
  );
  $form['content_scraper_config']['crawl']['content_scraper_scraping_crawl_parser'] = array(
    '#type' => 'select',
    '#title' => t('Crawl parser'),
    '#options' => $plugins_parser_options,
    '#default_value' => variable_get('content_scraper_scraping_crawl_parser', 'DrupalHttpParser'),
  );
  // Cron job.
  $form['content_scraper_config']['cron'] = array(
    '#type' => 'fieldset',
    '#weight' => 8,
    '#title' => t('Cron job'),
    '#group' => 'content_scraper_config',
  );
  $form['content_scraper_config']['cron']['content_scraper_scraping_cron_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Cronjob'),
    '#default_value' => variable_get('content_scraper_scraping_cron_enabled', FALSE),
  );
  $form['content_scraper_config']['cron']['content_scraper_scraping_cron_interval'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 10,
    '#title' => t('Cron interval:'),
    '#default_value' => variable_get('content_scraper_scraping_cron_interval', 1 * 60 * 60),
    '#states' => array(
      'invisible' => array(
        ':input[name="content_scraper_scraping_cron_enabled"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['content_scraper_config']['cron']['content_scraper_scraping_cron_limit'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 10,
    '#title' => t('Tasks limit:'),
    '#description' => t('Due to system memory and execution time limitations, please set this number carefully'),
    '#default_value' => variable_get('content_scraper_scraping_cron_limit', 20),
    '#states' => array(
      'invisible' => array(
        ':input[name="content_scraper_scraping_cron_enabled"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form = system_settings_form($form);
  $form['#submit'][] = '_content_scraper_scraping_config_form_callback';

  return $form;
}

/**
 * Settings form callback.
 * @param $form
 * @param $form_state
 */
function _content_scraper_scraping_config_form_callback($form, $form_state) {
  if (isset($form_state['values']['content_scraper_scraping_cron_enabled']) && $form_state['values']['content_scraper_scraping_cron_enabled'] == 0) {
    variable_set('content_scraper_scraping_cron_next_execution', 0);
    variable_set('content_scraper_scraping_cron_last_execution', 0);
  }
}

/**
 * @param $form
 * @param $form_state
 * @return array
 */
function content_scraper_tool($form, &$form_state) {
  $form = array();
  $form['content_scraper_tool'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => 'overview',
  );
  // Cron job.
  $form['content_scraper_tool']['scraper'] = array(
    '#type' => 'fieldset',
    '#weight' => 8,
    '#title' => t('Scraper'),
    '#group' => 'content_scraper_scraping',
  );
  $form['content_scraper_tool']['scraper']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Bulk fetch contents'),
    '#submit' => array('_content_scraper_scraping_bulk_form_submit'),
  );
  return $form;
}

/**
 * Bulk batch operations form submit handler.
 * @see content_scraper_bulk_form().
 * @param $form
 * @param $form_state
 */
function _content_scraper_scraping_bulk_form_submit($form, &$form_state) {
  // @TODO: Switch function regarding form_state values.
  $function = 'content_scraper_scraping_bulk_update_batch';
  $batch = $function();
  batch_set($batch);
}
