<?php

/**
 * @file
 * Provides helper function.
 */

/**
 * Delete content scraper instance.
 * @param $entity_type
 * @param null $bundle
 */
function content_scraper_delete_instances($entity_type, $bundle = NULL) {
  // Prepare a parameters array to load the specified instances.
  $params = array(
    'entity_type' => $entity_type,
  );

  if (!empty($bundle)) {
    $params['bundle'] = $bundle;
    // Delete this bundle's field bundle settings.
    variable_del('field_bundle_settings_' . $entity_type . '__' . $bundle);
  }
  else {
    // Delete all field bundle settings for this entity type.
    db_delete('variable')
      ->condition('name', db_like('field_bundle_settings_' . $entity_type . '__') . '%', 'LIKE')
      ->execute();
  }

  // Read and delete the matching field instances.
  foreach (field_read_instances($params, array('include_inactive' => TRUE)) as $instance) {
    content_scraper_delete_instance($instance);
  }
}

/**
 * Deletes the specified instance and handles field cleanup manually in case the
 * instance is of a disabled field.
 *
 * @param $instance
 *   The field instance info array to be deleted.
 */
function content_scraper_delete_instance($instance) {
  // First activate the instance's field if necessary.
  $field_name = $instance['field_name'];
  $activated = content_scraper_activate_field($field_name);

  // Clear the field cache if we just activated the field.
  if ($activated) {
    field_cache_clear();
  }

  // Then delete the instance.
  field_delete_instance($instance, TRUE);

  // Now check to see if there are any other instances of the field left.
  $field = field_info_field($field_name);

  if (count($field['bundles']) == 0) {
    field_delete_field($field_name);
  }
  elseif ($activated) {
    // If there are remaining instances but the field was originally disabled,
    // disabled it again now.
    $field['active'] = 0;
    field_update_field($field);
  }
}

/**
 * Finds all fields of a particular field type.
 * @param $field_type
 * @param null $entity_type
 * @return array
 */
function content_scraper_info_fields($field_type, $entity_type = NULL) {
  $fields = array();

  // Loop through the fields looking for any fields of the specified type.
  foreach (field_info_field_map() as $field_name => $field_stub) {
    if ($field_stub['type'] == $field_type) {
      // Add this field to the return array if no entity type was specified or
      // if the specified type exists in the field's bundles array.
      if (empty($entity_type) || in_array($entity_type, array_keys($field_stub['bundles']))) {
        $field = field_info_field($field_name);
        $fields[$field_name] = $field;
      }
    }
  }

  return $fields;
}

/**
 * Attempts to directly activate a field that was disabled due to its module
 * being disabled.
 *
 * The normal API function for updating fields, field_update_field(), will not
 * work on disabled fields. As a workaround, this function directly updates the
 * database, but it is up to the caller to clear the cache.
 *
 * @param $field_name
 *   The name of the field to activate.
 *
 * @return
 *   Boolean indicating whether or not the field was activated.
 */
function content_scraper_activate_field($field_name) {
  // Set it to active via a query because field_update_field() does
  // not work on inactive fields.
  $updated = db_update('field_config')
    ->fields(array('active' => 1))
    ->condition('field_name', $field_name, '=')
    ->condition('deleted', 0, '=')
    ->execute();

  return !empty($updated) ? TRUE : FALSE;
}

/**
 * Enables and deletes fields of the specified type.
 *
 * @param $type
 *   The type of fields to enable and delete.
 *
 * @see content_scraper_delete_field()
 */
function content_scraper_delete_fields($type) {
  // Read the fields for any active or inactive field of the specified type.
  foreach (field_read_fields(array('type' => $type), array('include_inactive' => TRUE)) as $field_name => $field) {
    content_scraper_delete_field($field_name);
  }
}

/**
 * Enables and deletes the specified field.
 *
 * The normal API function for deleting fields, field_delete_field(), will not
 * work on disabled fields. As a workaround, this function first activates the
 * fields of the specified type and then deletes them.
 *
 * @param $field_name
 *   The name of the field to enable and delete.
 */
function content_scraper_delete_field($field_name) {
  // In case the field is inactive, first activate it and clear the field cache.
  if (content_scraper_activate_field($field_name)) {
    field_cache_clear();
  }

  // Delete the field.
  field_delete_field($field_name);
}
