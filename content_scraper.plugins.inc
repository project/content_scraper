<?php

/**
 * @file
 * CTools plugins declarations.
 */

/**
 * Plugins.
 * @see content_scraper_content_scraper_plugins()
 */
function _content_scraper_plugins() {
  $path = drupal_get_path('module', 'content_scraper') . '/plugins';

  $info = array();
  $info['ScraperPlugin'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'class' => 'ScraperPlugin',
      'file' => 'ScraperPlugin.inc',
      'path' => $path,
    ),
  );
  $info['ScraperFetcher'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'parent' => 'ScraperPlugin',
      'class' => 'ScraperFetcher',
      'file' => 'ScraperFetcher.inc',
      'path' => $path,
    ),
  );
  $info['ScraperParser'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'parent' => 'ScraperPlugin',
      'class' => 'ScraperParser',
      'file' => 'ScraperParser.inc',
      'path' => $path,
    ),
  );
  $info['ScraperProcessor'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'parent' => 'ScraperPlugin',
      'class' => 'ScraperProcessor',
      'file' => 'ScraperProcessor.inc',
      'path' => $path,
    ),
  );
  $info['DrupalHttpFetcher'] = array(
    'name' => 'Drupal HTTP Fetcher',
    'description' => 'Fetches an URL content.',
    'handler' => array(
      'parent' => 'ScraperFetcher',
      'class' => 'DrupalHttpFetcher',
      'file' => 'DrupalHttpFetcher.inc',
      'path' => $path,
    ),
  );
  $info['SimpleParser'] = array(
    'name' => 'Simple Parser',
    'description' => 'Simple and default HTML content parser.',
    'handler' => array(
      'parent' => 'ScraperParser',
      'class' => 'SimpleParser',
      'file' => 'SimpleParser.inc',
      'path' => $path,
    ),
  );

  return $info;
}
