<?php

/**
 * @file
 * Provides controller class for entities.
 */

/**
 * Class ContentScraperContentEntityController
 */
class ContentScraperContentEntityController extends DrupalContentScraperEntityController {
  /**
   * @param array $values
   * @return object
   */
  public function create(array $values = array()) {
    /**
     * Default content values.
     */
    $values += array(
      'sid' => NULL,
      'is_new' => TRUE,
      'revision_id' => NULL,
      'label' => '',
      'uid' => '',
      'status' => 1,
      'created' => '',
      'changed' => '',
    );

    return parent::create($values);
  }

  /**
   * Save scraping content entity into database.
   * @param $entity
   * @param \DatabaseTransaction|NULL $transaction
   * @return bool|int
   * @throws \Exception
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    global $user;

    // Hard code the changed time.
    $entity->changed = REQUEST_TIME;

    if (empty($entity->{$this->idKey}) || !empty($entity->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($entity->created)) {
        $entity->created = REQUEST_TIME;
      }
    }
    else {
      if ($entity->created === '') {
        unset($entity->created);
      }
      if ($entity->uid === '') {
        unset($entity->uid);
      }
    }

    $entity->revision_timestamp = REQUEST_TIME;
    $entity->revision_uid = $user->uid;

    // Determine if we will be inserting a new content entity.
    $entity->is_new = empty($entity->sid);

    if ($entity->is_new || !empty($entity->revision)) {
      if (!isset($entity->log)) {
        // @todo default log message.
        $entity->log = '';
      }
    }
    elseif (empty($entity->log)) {
      unset($entity->log);
    }

    // Update link entity.
    $language = !empty($entity->language) ? $entity->language : LANGUAGE_NONE;
    if (!empty($entity->content_scraper_url[$language][0])) {
      $link = $entity->content_scraper_url[$language][0];
      // Load module inc file.
      module_load_include('inc', 'content_scraper', 'includes/content_scraper.handler');
      content_scraper_content_save_link($link, $entity);
    }

    return parent::save($entity, $transaction);
  }

  /**
   * Unserialize the data property of loaded entities.
   */
  public function attachLoad(&$queried_links, $revision_id = FALSE) {
    foreach ($queried_links as $link_id => &$entity) {
      $entity->data = unserialize($entity->data);
    }

    // Call the default attachLoad() method. This will add fields and call
    // hook_content_scraper_content_load().
    parent::attachLoad($queried_links, $revision_id);
  }

  /**
   * Delete scraping content entities.
   * @param $link_ids
   * @param \DatabaseTransaction|NULL $transaction
   * @return bool
   * @throws \Exception
   */
  public function delete($link_ids, DatabaseTransaction $transaction = NULL) {
    if (!empty($link_ids)) {
      // Load module inc file.
      module_load_include('inc', 'content_scraper', 'includes/content_scraper.handler');

      $entities = $this->load($link_ids, array());

      // Ensure the scraped content entity can actually be deleted.
      foreach ((array) $entities as $link_id => $entity) {
        if (!content_scraper_content_can_delete($entity)) {
          unset($entities[$link_id]);
        }
      }

      // If none of the specified entities can be deleted, return FALSE.
      if (empty($entities)) {
        return FALSE;
      }

      // Delete link entity mapping information and records.
      content_scraper_content_delete_link(array_keys($entities));

      parent::delete(array_keys($entities), $transaction);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}

/**
 * Class ContentScraperContentTypeEntityController
 */
class ContentScraperContentTypeController extends EntityAPIControllerExportable {
  /**
   * Create object for saving.
   */
  public function create(array $values = array()) {
    $values += array(
      'tid' => NULL,
      'name' => NULL,
      'label' => '',
      'description' => '',
      'weight' => 0,
      'data' => array(
        'format' => 0,
        'revision' => 1,
      ),
    );

    return parent::create($values);
  }

  /**
   * @param $entity
   * @param \DatabaseTransaction|NULL $transaction
   * @return bool|int
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    return parent::save($entity, $transaction);
  }
}
