<?php

/**
 * @file
 * Provides custom scraper view integration controller class for entities.
 */

/**
 * Class ContentScraperContentEntityViewsController
 */
class ContentScraperLinkEntityViewsController extends EntityDefaultViewsController {
  /**
   * Defines the result for hook_views_data().
   */
  public function views_data() {
    $data = parent::views_data();
    return $data;
  }
}
