<?php

/**
 * @file
 * Provides scraping content controller class for entities.
 */

/**
 * Class ContentScraperLinkEntityController
 */
class ContentScraperLinkEntityController extends DrupalContentScraperEntityController {
  /**
   * @param array $values
   * @return object
   */
  public function create(array $values = array()) {
    /**
     * Default content values.
     */
    $values += array(
      'link_id' => NULL,
      'is_new' => TRUE,
      'label' => '',
      'hash' => '',
      'uri' => '',
      'code' => '',
      'message' => '',
      'created' => '',
      'changed' => '',
      'checked_time' => '',
    );

    return parent::create($values);
  }

  /**
   * Save scraping content entity into database.
   * @param $link
   * @param \DatabaseTransaction|NULL $transaction
   * @return bool|int
   * @throws \Exception
   */
  public function save($link, DatabaseTransaction $transaction = NULL) {
    global $user;

    // Hard code the changed time.
    $link->changed = REQUEST_TIME;

    if (empty($link->{$this->idKey}) || !empty($link->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($link->created)) {
        $link->created = REQUEST_TIME;
      }
    }
    else {
      if ($link->created === '') {
        unset($link->created);
      }
      unset($link->uid);
    }

    $link->revision_timestamp = REQUEST_TIME;
    $link->revision_uid = $user->uid;

    // Determine if we will be inserting a new content entity.
    $link->is_new = empty($link->link_id);

    if ($link->is_new || !empty($link->revision)) {
      if (!isset($link->log)) {
        // @todo default log message.
        $link->log = '';
      }
    }
    elseif (empty($link->log)) {
      unset($link->log);
    }

    return parent::save($link, $transaction);
  }

  /**
   * Unserialize the data property of loaded entities.
   */
  public function attachLoad(&$queried_links, $revision_id = FALSE) {
    foreach ($queried_links as $link_id => &$link) {
      $link->data = unserialize($link->data);
    }

    // Call the default attachLoad() method. This will add fields and call
    // hook_content_scraper_entity_load().
    parent::attachLoad($queried_links, $revision_id);
  }

  /**
   * Delete scraping content entities.
   * @param $link_ids
   * @param \DatabaseTransaction|NULL $transaction
   * @return bool
   * @throws \Exception
   */
  public function delete($link_ids, DatabaseTransaction $transaction = NULL) {
    if (!empty($link_ids)) {
      $links = $this->load($link_ids, array());

      // Ensure the scraped content entity can actually be deleted.
      foreach ((array) $links as $link_id => $link) {
        if (!content_scraper_link_can_delete($link)) {
          unset($links[$link_id]);
        }
      }

      // If none of the specified entities can be deleted, return FALSE.
      if (empty($links)) {
        return FALSE;
      }

      parent::delete(array_keys($links), $transaction);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}
