<?php

/**
 * @file
 * Provides entity controller class for entities.
 */

/**
 * Class ContentScraperContentEntity
 */
class ContentScraperContentEntity extends Entity {
  protected function defaultUri() {
    return array('path' => 'scraper/content/' . $this->identifier());
  }
}

/**
 * Class ContentScraperContentEntity
 */
class ContentScraperContentTypeEntity extends Entity {
  public function __construct($values = array()) {
    parent::__construct($values, 'content_scraper_content_type');
  }

  protected function defaultUri() {
    return array('path' => 'scraper/type/' . $this->identifier());
  }
}
