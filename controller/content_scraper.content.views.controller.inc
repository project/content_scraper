<?php

/**
 * @file
 * Provides custom scraper view integration controller class for entities.
 */

/**
 * Class ContentScraperContentEntityViewsController
 * @see content_scraper_content_entity_property_info()
 */
class ContentScraperContentEntityViewsController extends EntityDefaultViewsController {
  /**
   * Defines the result for hook_views_data().
   */
  public function views_data() {
    $data = parent::views_data();
    // Expose User ID.
    $data['content_scraper_content']['uid'] = array(
      'title' => t('UID'),
      'help' => t('The unique ID of the scraped content creator.'),
      'field' => array(
        'handler' => 'views_handler_field_user',
        'click sortable' => TRUE,
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_user_uid',
        'name field' => 'name', // display this field in the summary
      ),
      'filter' => array(
        'title' => t('Name'),
        'handler' => 'views_handler_filter_user_name',
      ),
      'relationship' => array(
        'title' => t('Creator'),
        'help' => t("Relate this content entity to its creator's user account"),
        'handler' => 'views_handler_relationship',
        'base' => 'users',
        'base field' => 'uid',
        'field' => 'uid',
        'label' => t('Content scraper content creator'),
      ),
    );
    // Join content_scraper_link_mapping table.
    $data['content_scraper_content']['table']['join']['content_scraper_link'] = array(
      'left_table' => 'content_scraper_link_mapping',
      'left_field' => 'sid',
      'field' => 'entity_id',
      'extra' => array(
        array('field' => 'entity_type', 'value' => 'content_scraper_content'),
      ),
    );
    $data['content_scraper_link_mapping']['table']['join']['content_scraper_link'] = array(
      'left_field' => 'link_id',
      'field' => 'link_id',
    );
    // Define the relationship from content_scraper_content to content_scraper_link_mapping.
    $data['content_scraper_content']['content_scraper_link_mapping']['relationship'] = array(
      'title' => t('Link mapping'),
      'help' => t("Relate this entity to a link."),
      'handler' => 'views_handler_relationship',
      'base' => 'content_scraper_link_mapping',
      'base field' => 'entity_id',
      'field' => 'sid',
      'label' => t('Link mapping', array(), array('context' => 'a link mapping with this entity')),
    );
    return $data;
  }
}
