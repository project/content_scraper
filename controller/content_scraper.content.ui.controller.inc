<?php

/**
 * @file
 * Provides UI controller class for entities.
 */

/**
 * Class ContentScraperContentEntityUIController
 */
class ContentScraperContentEntityUIController extends EntityDefaultUIController {

  /**
   * Defines the number of entries to show per page in overview table.
   */
  public $overviewPagerLimit = 50;

  public function __construct($entity_type, $entity_info) {
    $this->entityType = $entity_type;
    $this->entityInfo = $entity_info;
    $this->path = $this->entityInfo['admin ui']['path'];
    $this->statusKey = empty($this->entityInfo['entity keys']['status']) ? 'status' : $this->entityInfo['entity keys']['status'];
  }

  /**
   * Provides definitions for implementing hook_menu().
   */
  public function hook_menu() {
    $menus = parent::hook_menu();
    return $menus;
  }

  /**
   * @param $form
   * @param $form_state
   * @return mixed
   */
  public function overviewForm($form, &$form_state) {
    $form['filter'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Filter option')
    );
    $form['filter']['filter_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#size' => 15,
    );
    $form['filter']['filter_url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#size' => 15,
    );
    $form['filter']['filter_code'] = array(
      '#type' => 'textfield',
      '#title' => t('HTTP code'),
      '#size' => 15,
    );
    $form['filter']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Filter'),
    );
    $form['filter']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );
    $form['bulk_operations'] = array(
      '#type' => 'fieldset',
      '#title' => t('Bulk Operations'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['bulk_operations']['operations'] = array(
      '#type' => 'select',
      '#options' => array(
        '0' => t('Select a bulk operation'),
        'delete' => t('Delete selected entities'),
      ),
    );

    $form['bulk_operations']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
    $form['table'] = $this->overviewTable();
    $form['pager'] = array('#theme' => 'pager');
    return $form;
  }

  /**
   * @param $form
   * @param $form_state
   */
  public function overviewFormSubmit($form, &$form_state) {
    $queryString = array();
    if (isset($form_state['input']) && is_array($form_state['input'])) {
      $values = $form_state['input'];
      if (isset($values['op'])) {
        switch ($values['op']) {
          case 'Filter':
            if (!empty($values['filter_title'])) {
              $queryString['filter']['title'] = check_plain($values['filter_title']);
            }
            if (!empty($values['filter_url'])) {
              $queryString['filter']['url'] = check_plain($values['filter_url']);
            }
            if (!empty($values['filter_code'])) {
              $queryString['filter']['code'] = check_plain($values['filter_code']);
            }
            break;
          case 'Submit':
            $scraper_ids = array();
            if (!empty($values['tablesort_table'])) {
              foreach ($values['tablesort_table'] as $index => $value) {
                if (!empty($value)) {
                  $scraper_ids[] = str_replace('content-scraper-id-', '', $value);
                }
              }
            }
            if (!empty($scraper_ids)) {
              if ($values['operations'] == 'delete') {
                content_scraper_content_delete_multiple($scraper_ids);
              }
            }
            break;
        }
      }
    }

    $form_state['redirect'] = array(
      $this->path,
      array(
        'query' => $queryString,
      )
    );
  }

  /**
   * @param array $conditions
   * @return array
   */
  public function overviewTable($conditions = array()) {
    // Sortable overview table headers.
    $headers_sortable = array(
      array(
        'data' => t('Title'),
        'field' => 'csc.sid',
        'sort' => 'desc'
      ),
      array(
        'data' => t('Type'),
        'field' => 'csc.type'
      ),
      array(
        'data' => t('Status'),
        'field' => 'csc.status'
      ),
      array(
        'data' => t('HTTP code'),
        'field' => 'csl.code'
      ),
      array(
        'data' => t('HTTP message'),
        'field' => 'csl.message'
      ),
      array(
        'data' => t('Last checked'),
        'field' => 'csl.checked_time'
      ),
    );

    $q_sub_links = db_select('content_scraper_link', 'csl');
    $q_sub_links->fields('csl', array(
      'link_id',
      'uri',
      'code',
      'message',
      'checked_time'
    ));

    $q_sub_mapping = db_select('content_scraper_link_mapping', 'cslm');
    $q_sub_mapping->join($q_sub_links, 'csl', 'csl.link_id = cslm.link_id AND cslm.entity_type = :entity_type', array(
      ':entity_type' => 'content_scraper_content'
    ));
    $q_sub_mapping->fields('cslm', array(
      'link_id',
      'entity_id',
      'entity_type',
    ));
    $q_sub_mapping->fields('csl', array(
      'link_id',
      'uri',
      'code',
      'message',
      'checked_time'
    ));

    $q_sub_content = db_select('content_scraper_content', 'csc')
      ->extend('TableSort')
      ->extend('PagerDefault');
    // Set page limit.
    if ($this->overviewPagerLimit) {
      $q_sub_content->limit($this->overviewPagerLimit);
    }
    $q_sub_content->leftJoin($q_sub_mapping, 'csl', 'csc.sid = csl.entity_id AND csl.entity_type = :entity_type', array(
      ':entity_type' => 'content_scraper_content'
    ));
    $q_sub_content->fields('csc', array(
      'sid',
      'label',
      'type',
      'status'
    ));
    $q_sub_content->fields('csl', array(
      'uri',
      'code',
      'message',
      'checked_time'
    ));

    // Get filter.
    $filter = !empty($_GET['filter']) ? $_GET['filter'] : array();

    if (!empty($filter['code']) && !is_null($filter['code'])) {
      $q_sub_content->condition('code', '%' . db_like($filter['code']) . '%', 'like');
    }

    if (!empty($filter['title']) && !is_null($filter['title'])) {
      $q_sub_content->condition('label', '%' . db_like($filter['title']) . '%', 'like');
    }

    if (!empty($filter['url']) && !is_null($filter['url'])) {
      $q_sub_content->condition('uri', '%' . db_like($filter['url']) . '%', 'like');
    }

    // Run query.
    $result = $q_sub_content
      ->orderByHeader($headers_sortable)
      ->execute();

    $rows = array();
    foreach ($result as $row) {
      $rows['content-scraper-id-' . $row->sid] = $this->overviewTableRow($conditions, $row->sid, $row);
    }

    // Build the table for the nice output.
    $build['tablesort_table'] = array(
      '#type' => 'tableselect',
      '#header' => $this->overviewTableHeaders($conditions, $rows, $headers_sortable),
      '#options' => $rows,
      '#empty' => t('No content available.'),
      '#attributes' => array('class' => array('entity-sort-table')),
    );

    return $build;
  }

  /**
   * Build overview table headers.
   * @param $conditions
   * @param $rows
   * @param array $additional_header
   * @return array
   */
  public function overviewTableHeaders($conditions, $rows, $additional_header = array()) {
    $header = $additional_header;
    if (!empty($this->entityInfo['exportable'])) {
      $header[] = t('Status');
    }
    // Add operations with the right colspan.
    $header[] = array(
      'data' => t('Operations'),
      'colspan' => $this->operationCount()
    );
    return $header;
  }

  /**
   * Build overview table row.
   * @param $conditions
   * @param $id
   * @param $entity
   * @param array $additional_cols
   * @return array
   */
  public function overviewTableRow($conditions, $id, $entity, $additional_cols = array()) {
    // Load rows.
    foreach ($entity as $k => $col) {
      if ($k == 'sid' || $k == 'uri') {
        continue;
      }
      if ($k == 'type') {
        $col = content_scraper_content_type_get_name($col);
      }
      if ($k == 'status') {
        $col = !empty($col) && $col == 1 ? 'Active' : 'Disabled';
      }
      if ($k == 'label') {
        $col = empty($col) ? 'URL empty' : l($col, check_url($entity->uri));
      }
      if ($k == 'checked_time') {
        $col = empty($col) ? 'Pending' : format_date($col, 'custom', 'D, m/d/Y - H:i');
      }
      $row[] = $col;
    }

    // Add in any passed additional cols.
    foreach ($additional_cols as $col) {
      $row[] = $col;
    }

    $operations[] = l(t('edit'), $this->path . '/manage/' . $id);

    if (empty($this->entityInfo['exportable']) || !entity_has_status($this->entityType, $entity, ENTITY_IN_CODE)) {
      $operations[] = l(t('delete'), $this->path . '/manage/' . $id . '/delete', array('query' => drupal_get_destination()));
    }
    elseif (entity_has_status($this->entityType, $entity, ENTITY_OVERRIDDEN)) {
      $operations[] = l(t('revert'), $this->path . '/manage/' . $id . '/revert', array('query' => drupal_get_destination()));
    }

    $opt_output = '<ul class="inline links">';
    foreach ($operations as $opt) {
      $opt_output .= '<li>' . $opt . '</li>';
    }
    $opt_output .= '</ul>';

    $row[] = $opt_output;

    // Apply class depending on status.
    $http_code = !empty($entity->code) ? $entity->code : 0;
    // Load module inc file.
    module_load_include('inc', 'content_scraper', 'includes/content_scraper.func');
    $row_class = content_scraper_content_admin_ui_code($http_code);
    $row['#attributes'] = array('class' => array($row_class));
    return $row;
  }
}

class ContentScraperContentTypeEntityUIController extends EntityDefaultUIController {

}
