<?php

/**
 * @file
 * Provides Entity metadata integration.
 */

 /**
 * Implements hook_entity_property_info().
 */
function content_scraper_entity_property_info() {
  $info = array();
  $properties = &$info['content_scraper_link']['properties'];

  $properties['link_id'] = array(
    'label' => t('Scraped content link ID'),
    'description' => t('The internal numeric ID of the scraped content link.'),
    'type' => 'integer',
    'schema field' => 'link_id',
  );

  $properties['type'] = array(
    'label' => t('Type'),
    'description' => t('The type of the scraped content.'),
    'type' => 'token',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer content scraper type',
    'required' => TRUE,
    'schema field' => 'type',
  );

  $properties['label'] = array(
      'label' => t('Label'),
      'description' => t('The label of scraped content.'),
      'type' => 'text',
      'setter callback' => 'entity_property_verbatim_set',
      'required' => TRUE,
      'schema field' => 'label',
    );

  $properties['language'] = array(
    'label' => t('Language'),
    'type' => 'token',
    'description' => t('The language the scraped content was created in.'),
    'setter callback' => 'entity_property_verbatim_set',
    'options list' => 'entity_metadata_language_list',
    'schema field' => 'language',
    'setter permission' => 'administer content scraper content',
  );

  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the scraped content was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer content scraper content',
    'schema field' => 'created',
  );

  $properties['changed'] = array(
    'label' => t('Date updated'),
    'description' => t('The date the scraped content was most recently updated.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'query callback' => 'entity_metadata_table_query',
    'setter permission' => 'administer content scraper content',
    'schema field' => 'changed',
  );

  $info['content_scraper_link']['bundles'] = array(
    'internal' => array(
      'label' => t('internal'),
    ),
    'external' => array(
      'label' => t('external'),
    ),
  );

  $properties = &$info['content_scraper_content']['properties'];

  $properties['sid'] = array(
    'label' => t('Scraped content ID'),
    'description' => t('The internal numeric ID of the scraped content.'),
    'type' => 'integer',
    'schema field' => 'sid',
  );

  $properties['type'] = array(
    'label' => t('Type'),
    'description' => t('The type of the scraped content.'),
    'type' => 'token',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer content scraper type',
    'options list' => 'content_scraper_content_type_options_list',
    'required' => TRUE,
    'schema field' => 'type',
  );

  $properties['label'] = array(
      'label' => t('Label'),
      'description' => t('The label of scraped content.'),
      'type' => 'text',
      'setter callback' => 'entity_property_verbatim_set',
      'required' => TRUE,
      'schema field' => 'label',
    );

  $properties['language'] = array(
    'label' => t('Language'),
    'type' => 'token',
    'description' => t('The language the scraped content was created in.'),
    'setter callback' => 'entity_property_verbatim_set',
    'options list' => 'entity_metadata_language_list',
    'schema field' => 'language',
    'setter permission' => 'administer content scraper content',
  );

  $properties['status'] = array(
    'label' => t('Status'),
    'description' => t('Boolean indicating whether the scraped content is active or disabled.'),
    'type' => 'boolean',
    'options list' => 'content_scraper_content_status_options_list',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer content scraper content',
    'schema field' => 'status',
  );

  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date the scraped content was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer content scraper content',
    'schema field' => 'created',
  );

  $properties['changed'] = array(
    'label' => t('Date updated'),
    'description' => t('The date the scraped content was most recently updated.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'query callback' => 'entity_metadata_table_query',
    'setter permission' => 'administer content scraper content',
    'schema field' => 'changed',
  );

  $properties['uid'] = array(
    'label' => t('Creator ID'),
    'type' => 'integer',
    'description' => t('The unique ID of the scraped content creator.'),
    'setter callback' => 'entity_property_verbatim_set',
    'setter permission' => 'administer content scraper content',
    'clear' => array('creator'),
    'schema field' => 'uid',
  );

  $info['content_scraper_content']['bundles'] = array();
  foreach (content_scraper_content_type_get_name() as $type => $name) {
    $info['content_scraper_content']['bundles'][$type] = array(
      'label' => $name,
    );
  }

  return $info;
}

/**
 * Implements hook_entity_property_info_alter() on top of the Content scraper entity module.
 */
function content_scraper_entity_property_info_alter(&$info) {
  $properties = array();

  // Get bundle properties.
  foreach ($info['content_scraper_content']['bundles'] as $bundle => $bundle_info) {
    $bundle_info += array('properties' => array());
    $properties += $bundle_info['properties'];
  }
}
